import { ResponseOpenMeteo } from "@/types/api/OpenMeteo"
import { mapRange } from "./math"

export const averageValueFromList = (list: number[]): number =>
  list.reduce((acc, curr) => acc + curr, 0) / list.length

export const convertRawValuesToAverageValues = (
  hourlyValues: ResponseOpenMeteo["hourly"]
) => {
  return {
    temperature_2m: averageValueFromList(hourlyValues.temperature_2m),
    precipitation: averageValueFromList(hourlyValues.precipitation),
    rain: averageValueFromList(hourlyValues.rain),
    snowfall: averageValueFromList(hourlyValues.snowfall),
    cloudcover: convertCloudCoverToDbScale(
      averageValueFromList(hourlyValues.cloudcover)
    ),
    windspeed_10m: averageValueFromList(hourlyValues.windspeed_10m),
    soil_temperature_0_to_7cm: convertSoilHardiness(
      averageValueFromList(hourlyValues.soil_temperature_0_to_7cm)
    ),
    soil_moisture_0_to_7cm: convertAverageMoistureToDbScale(
      averageValueFromList(hourlyValues.soil_moisture_0_to_7cm)
    )
  }
}

export const convertCloudCoverToDbScale = (cloudCover: number): number => {
  const mappedCloudCover = mapRange(cloudCover, 0, 100, 0, 9)

  const rangeScale = [
    [1, 12, 13],
    [2, 23]
  ]

  if (mappedCloudCover < 4) {
    const hortipediaScale = Math.round(mapRange(mappedCloudCover, 0, 9, 0, 2))
    return rangeScale[0][hortipediaScale]
  }
  if (mappedCloudCover < 8.5) {
    const hortipediaScale = Math.round(mapRange(mappedCloudCover, 4, 8.5, 0, 1))
    return rangeScale[1][hortipediaScale]
  }

  return 3
}

export const convertAverageMoistureToDbScale = (moisture: number): number => {
  if (moisture <= 0.1) return 1
  if (moisture <= 0.25) return 2
  if (moisture <= 0.5) return 3
  if (moisture <= 0.6) return 4
  return 5
}

// A Vérifier si tout est good
export const convertSoilHardiness = (soilHardiness: number): number => {
  if (soilHardiness < -45) return 1 // DE - 45 à -l'infini
  if (soilHardiness >= -45 && soilHardiness < -40) return 2 // DE - 45 à -40
  if (soilHardiness >= -40 && soilHardiness < -35) return 3 // DE - 40 à -35
  if (soilHardiness >= -35 && soilHardiness < -29) return 4 // DE - 35 à -30

  if (soilHardiness >= -29 && soilHardiness < -23) return 5 // DE -29 à -23
  if (soilHardiness >= -23 && soilHardiness < -18) return 6 // DE -23 à -18
  if (soilHardiness >= -18 && soilHardiness < -12) return 7 // DE -18 à -12
  if (soilHardiness >= -12 && soilHardiness < -7) return 8 // DE -12 à -7

  if (soilHardiness >= -7 && soilHardiness < 1) return 9 // DE -7 à 1

  return 10
}
