import { HortipedaTableWithWeightList } from "@/types/api/Monstera"
import { HortipedaTable } from "@/types/database/Hortipeda"

export type HortipedaTableWithWeight = HortipedaTable & { weight: number }
type QueriesResult = { rows: HortipedaTable[]; weight: number }[]

export const sortingQueriesDatabase = (
  queriesResult: QueriesResult
): HortipedaTableWithWeightList => {
  const hortipediaUniqueTable = new Map<string, HortipedaTableWithWeight>()

  console.log(
    "queriesResult length total of rows",
    queriesResult.map(({ rows }) => rows.length).reduce((a, b) => a + b, 0)
  )
  queriesResult.forEach(({ rows, weight }, index) => {
    rows.forEach((row) => {
      if (hortipediaUniqueTable.has(row.name)) {
        const currentWeight = hortipediaUniqueTable.get(row.name)!.weight
        hortipediaUniqueTable.set(row.name, {
          ...row,
          weight: currentWeight + weight
        })
      } else {
        hortipediaUniqueTable.set(row.name, { ...row, weight })
      }
    })
  })

  const sortedHortipediaUniqueTable = new Map<string, HortipedaTableWithWeight>(
    [...hortipediaUniqueTable.entries()].sort((a, b) => {
      return b[1].weight - a[1].weight
    })
  )

  return [...sortedHortipediaUniqueTable.values()]
}
