import type { ResponseOpenMeteo } from "../types/api/OpenMeteo"
import {
  averageValueFromList,
  convertRawValuesToAverageValues,
  convertCloudCoverToDbScale,
  convertAverageMoistureToDbScale,
  convertSoilHardiness
} from "./processing"

describe("[Processing] unit tests", () => {
  it("Should return the average value of a list of numbers", () => {
    const actualResult = averageValueFromList([1, 2, 3, 4, 5])
    const expectedResult = 3
    expect(actualResult).toEqual(expectedResult)
  })

  it("Should return the average value of an object", () => {
    const actualResult: ResponseOpenMeteo["hourly"] = {
      temperature_2m: [1, 2, 3, 4, 5],
      precipitation: [0, 1, 3, 5],
      rain: [0, 1, 3, 5],
      snowfall: [0, 1, 3, 5],
      cloudcover: [50, 50, 50],
      windspeed_10m: [0, 1, 3, 5],
      soil_temperature_0_to_7cm: [0, 1, 3, 5],
      soil_moisture_0_to_7cm: [0, 1, 3, 5],
      time: []
    }
    const expectedResult = {
      temperature_2m: 3,
      cloudcover: 2,
      precipitation: 2.25,
      rain: 2.25,
      snowfall: 2.25,
      soil_moisture_0_to_7cm: 5,
      soil_temperature_0_to_7cm: 10,
      windspeed_10m: 2.25
    }
    expect(convertRawValuesToAverageValues(actualResult)).toEqual(
      expectedResult
    )
  })
  it("Should convert cloud cover to db scale", () => {
    expect(convertCloudCoverToDbScale(0)).toEqual(1)
    expect(convertCloudCoverToDbScale(60)).toEqual(2)
    expect(convertCloudCoverToDbScale(100)).toEqual(3)
  })
  it("Should convert moisture value to db scale", () => {
    expect(convertAverageMoistureToDbScale(0)).toEqual(1)
    expect(convertAverageMoistureToDbScale(0.25)).toEqual(2)
    expect(convertAverageMoistureToDbScale(0.5)).toEqual(3)
    expect(convertAverageMoistureToDbScale(0.6)).toEqual(4)
    expect(convertAverageMoistureToDbScale(0.7)).toEqual(5)
  })
  it("Should convert soil hardiness to db scale", () => {
    expect(convertSoilHardiness(-65)).toEqual(1)
    expect(convertSoilHardiness(-45)).toEqual(2)
    expect(convertSoilHardiness(-39)).toEqual(3)
    expect(convertSoilHardiness(-32)).toEqual(4)
    expect(convertSoilHardiness(-28)).toEqual(5)
    expect(convertSoilHardiness(-22)).toEqual(6)
    expect(convertSoilHardiness(-17)).toEqual(7)
    expect(convertSoilHardiness(-12)).toEqual(8)
    expect(convertSoilHardiness(-6)).toEqual(9)
    expect(convertSoilHardiness(1)).toEqual(10)
  })
})
