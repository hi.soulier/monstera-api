import dynamic from "next/dynamic"
import Head from "next/head"
import Header from "@/components/Header"

const MapLeafLet = dynamic(() => import("@/components/Map"), {
  ssr: false
})

const MapWrapper = () => {
  return (
    <>
      <section>
        <Header />

        {MapLeafLet ? <MapLeafLet /> : <div>Loading</div>}
      </section>
    </>
  )
}

export default MapWrapper
