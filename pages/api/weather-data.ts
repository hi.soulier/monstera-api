import {
  formatParamsForOpenMeteoQuery,
  requestOpenMeteo
} from "@/lib/open-meteo"
import {
  WeatherDataRequest,
  WeatherDataResponse
} from "@/types/api/WeatherData"
import { averageValueFromList } from "@/utils/processing"

/**
 * @swagger
 * /api/weather-data:
 *  post:
 *    description: Returns the weather data
 *    responses:
 *        200:
 *          description: weather data
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/models/openapi/schemas/api/ResponseOpenMeteo'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              coordinates:
 *                type: array
 *                items:
 *                  type: number
 */
const handler = async (req: WeatherDataRequest, res: WeatherDataResponse) => {
  if (req.method !== "POST")
    return res.status(405).end(`Method ${req.method} Not Allowed`)

  const [latitude, longitude] = req.body.coordinates

  const params = formatParamsForOpenMeteoQuery([latitude, longitude])

  const responseWeatherApi = await requestOpenMeteo(params)

  if (!responseWeatherApi?.data) {
    return res.status(500).json({ error: "No data from weather API" })
  }

  return res.status(200).json(responseWeatherApi.data)
}

export default handler
