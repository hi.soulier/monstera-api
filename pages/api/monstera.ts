import axios, { type AxiosResponse } from "axios"
import type { RequestOpenMeteo, ResponseOpenMeteo } from "@/types/api/OpenMeteo"
import { convertRawValuesToAverageValues } from "@/utils/processing"
import { formatParamsForOpenMeteoQuery } from "@/lib/open-meteo"
import { MonsteraRequest } from "@/types/api/Monstera"
import { queryResultDatabase } from "@/lib/query"

/**
 * @swagger
 * /api/monstera:
 *  post:
 *    description: Returns the monstera data
 *    responses:
 *        200:
 *          description: monstera data
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/api/HortipedaTableWithWeightList'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              coordinates:
 *                type: array
 *                items:
 *                  type: number
 *              soil_type:
 *                type: string
 */
const handler = async (req: MonsteraRequest, res: any) => {
  if (req.method !== "POST") {
    res.status(405).end(`Method ${req.method} Not Allowed`)
    return
  }
  if (!req.body) {
    res.status(400).end(`No body`)
    return
  }

  const { coordinates, soil_type } = req.body

  const params = formatParamsForOpenMeteoQuery(coordinates)

  const responseWeatherApi = await axios
    .get<ResponseOpenMeteo, AxiosResponse<ResponseOpenMeteo>, RequestOpenMeteo>(
      "https://archive-api.open-meteo.com/v1/era5",
      { params }
    )
    .catch((err) => console.warn(err.response))

  if (!responseWeatherApi?.data) {
    return res.status(500).json({ error: "No data from weather API" })
  }

  const averageValues = convertRawValuesToAverageValues(
    responseWeatherApi.data.hourly
  )

  // const [queryMoisture, queryCloudCoverage, queryTemperature] =
  //   await Promise.all(
  //     moistureCloudCoverTemperatureQueries({
  //       moistureValue: averageValues.soil_moisture_0_to_7cm,
  //       exposureValue: averageValues.cloudcover,
  //       hardinessValue: averageValues.soil_temperature_0_to_7cm
  //     })
  //   )

  const queryResultDb = await queryResultDatabase({
    moistureValue: averageValues.soil_moisture_0_to_7cm,
    exposureValue: averageValues.cloudcover,
    hardinessValue: averageValues.soil_temperature_0_to_7cm
  })

  res.status(200).json(
    queryResultDb.rows
    // sortingQueriesDatabase([
    //   { rows: queryMoisture.rows, weight: DEFAULT_WEIGHT },
    //   { rows: queryCloudCoverage.rows, weight: DEFAULT_WEIGHT },
    //   { rows: queryTemperature.rows, weight: DEFAULT_WEIGHT }
    // ])
  )
}

export default handler
