import React from "react"
import Header from "@/components/Header"
import Link from "next/link"
import { useState, useEffect } from "react"
import Image from "next/image"

import leaf_r from "/public/leaf_r.png"
import monsteraLanding from "/public/monstera2.png"

type Props = void

const home = (props: Props) => {
  const [leafanime, setLeafanime] = useState<boolean>(false)

  useEffect(() => {
    setTimeout(() => {
      setLeafanime(true)
    }, 1000)
  }, [])
  return (
    <>
      <section className="home">
        <div className="leaf-wrapper">
          <Image
            className={leafanime ? "leafleft leafleft2" : "leafleft"}
            src={leaf_r}
            alt="leaf_r"
            loading="eager"
          />
          <Image
            className={leafanime ? "leafright leafright2" : "leafright"}
            src={leaf_r}
            alt="leaf_r"
            loading="eager"
          />
          <Image
            className={leafanime ? "leaftopleft leaftopleft2" : "leaftopleft"}
            src={leaf_r}
            alt="leaf_r"
            loading="eager"
          />
          <Image
            className={leafanime ? "leaftop leaftop2" : "leaftop"}
            src={leaf_r}
            alt="leaf_r"
            loading="eager"
          />
          <Image
            className={leafanime ? "leaftop leaftop2" : "leaftop"}
            src={leaf_r}
            alt="leaf_r"
            loading="eager"
          />
          <Image
            className={
              leafanime ? "leaftopright leaftopright2" : "leaftopright"
            }
            src={leaf_r}
            alt="leaf_r"
            loading="eager"
          />
          <Image
            className={leafanime ? "leafbotleft leafbotleft2" : "leafbotleft"}
            src={leaf_r}
            alt="leaf_r"
            loading="eager"
          />
          <Image
            className={leafanime ? "leafbot leafbot2" : "leafbot"}
            src={leaf_r}
            alt="leaf_r"
            loading="eager"
          />
        </div>
        <div className={leafanime ? "opacityAnime2" : "opacityAnime"}>
          <Header />
          <div className=" w-screen h-screen">
            <div
              className="absolute z-0"
              style={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)"
              }}
            >
              <section className="content">
                <div className="monstera">
                  <Image
                    className="landing-img"
                    src={monsteraLanding}
                    alt="monstera in a pot"
                  />
                  <h2 className="title">Monstera</h2>
                </div>
                <div className="wrap">
                  <Link href="/project">
                    <button className="go-btn">Go!</button>
                  </Link>
                </div>
                <span className="text-center mt-5 mb-5 font-[100]">
                  Commencer votre projet
                </span>
                {/* <div className="about-us">
                  <Link href="/about-us">
                    <div className="about-div">
                      <img
                        src="/chevron.svg"
                        alt="about us"
                        className="about-chevron"
                      />
                      <p className="about-paraph">À propos de nous</p>
                    </div>
                    <img src="/monstera3.png" alt="" className="about-image" />
                  </Link>
                </div> */}
              </section>

              <div className="description">
                <h3 className="description-title">Votre jardinier personnel</h3>
                <p className="description-paraph">à la maison</p>
              </div>

              <div className="add-plant">
                <a
                  href="https://fr.hortipedia.com/Accueil"
                  className="cursor-pointer"
                >
                  <h3 className="description-title">Se renseigner sur</h3>
                  <div className="flex flex-row items-center relative justify-end	gap-[0.1vw]">
                    <span className="block w-9 h-1 bg-gray-600"></span>
                    <p className="dd-plant_desc">Hortipedia!</p>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default home
