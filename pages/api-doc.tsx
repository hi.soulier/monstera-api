import Header from "@/components/Header"
import { GetStaticProps, InferGetStaticPropsType } from "next"
import { createSwaggerSpec } from "next-swagger-doc"
import dynamic from "next/dynamic"
import "swagger-ui-react/swagger-ui.css"
const SwaggerUI = dynamic(import("swagger-ui-react"), { ssr: false })

function ApiDoc({ spec }: InferGetStaticPropsType<typeof getStaticProps>) {
  return (
    <>
      <Header />
      <SwaggerUI spec={spec} />
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const spec: Record<string, any> = createSwaggerSpec({
    apiFolder: "pages/api",
    schemaFolders: ["models"],
    definition: {
      openapi: "3.0.0",
      servers: [
        {
          url: "http://localhost:3000",
          description: "Local server"
        },
        {
          url: "https://server.com",
          description: "Production server"
        }
      ],
      info: {
        description: "Write the description",
        version: "0.1",
        title: "Monstera API",
        license: {
          name: "MIT",
          url: "https://opensource.org/licenses/MIT"
        }
      }
    }
  })

  return {
    props: {
      spec
    }
  }
}

export default ApiDoc
