import Header from "@/components/Header"
import Formulaire from "@/components/Formulaire"
import React from "react"
import Link from "next/link"

const project = () => {
  return (
    <>
      <Header />
      <section className="project-content">
        <Formulaire />
      </section>
      <Link href="/map">
        <div className="project-ignore">
          <span className="project-circle"></span>
          <p className="project-ignore-title">Ignorer ces questions</p>
          <img src="/Arrow.svg" alt="ignore all questions" />
        </div>
      </Link>
    </>
  )
}

export default project
