import React from "react"
import Header from "@/components/Header"

type Props = void

const aboutUs = (props: Props) => {
  return (
    <section>
      <Header />
      ABOUT US
    </section>
  )
}

export default aboutUs
