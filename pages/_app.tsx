import type { AppProps } from "next/app"
import Head from "next/head"
import { Inter } from "@next/font/google"
import "../style/global.css"
import { useRouter } from "next/router"

const inter = Inter({
  subsets: ["latin"],
  variable: "--font-sans"
})

export default function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter()
  return (
    <>
      <Head>
        <title>Monstera</title>
        <meta property="og:title" content="Monstera API" key="Monstera" />
      </Head>
      <main
        className={`${inter.variable} font-sans ${
          router.asPath.includes("/api-doc") ? "swagger" : ""
        }`}
      >
        <Component {...pageProps} />
      </main>
    </>
  )
}
