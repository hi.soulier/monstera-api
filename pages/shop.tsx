import Header from "@/components/Header"

const shop = () => {
  return (
    <section>
      <Header />
      <section className="content">
        <h2 className="shop-title">terrarium kit</h2>
        <section className="side">
          <div className="shop-wrap">
            <div className="flex items-center gap-[1vw]">
              <h3 className="shop-price">Prix</h3>
              <p className="block w-9 h-1 bg-gray-600"></p>
            </div>
            <p className="desc">39,99€</p>
          </div>
          <div className="shop-wrap">
            <div className="flex items-center gap-[1vw]">
              <h3 className="shop-price">Description</h3>
              <p className="block w-9 h-1 bg-gray-600"></p>
            </div>
            <p className="desc">
              Kit contenant des graines et de la terre local
            </p>
          </div>
        </section>
        <img
          src="/bac.png"
          alt="pot to grow your monstera"
          className="shop-image"
        />
        <div>
          <button className="buy-btn">
            <a href="#">Acheter</a>
          </button>
        </div>
      </section>
    </section>
  )
}

export default shop
