import React, { useState } from "react"
import * as L from "leaflet"
import "leaflet/dist/leaflet.css"
import type { ResponseOpenMeteo } from "../types/api/OpenMeteo"
import Loader from "./Loader"
import { MapContainer, Marker, TileLayer, useMapEvents } from "react-leaflet"
import { SoilTypeList } from "@/types/Lists"
import Link from "next/link"

export type Payload = {
  coordinates: [number, number]
  soil_type: SoilTypeList
}

const sendRequest = async (payload: Payload) => {
  const res = await fetch("/api/monstera", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(payload)
  })
  const data = (await res.json()) as ResponseOpenMeteo
  console.log(
    "req",
    {
      lat: payload.coordinates[0],
      lng: payload.coordinates[1],
      soilType: payload.soil_type
    },
    "res",
    { lat: data.latitude, lng: data.longitude, soil_type: data.soil_type }
  )

  return data
}

const randomObjectsFromArray = (array: any[]): any[] => {
  const randomIndexes: number[] = []
  const objects: any[] = []

  while (randomIndexes.length < 3) {
    const randomIndex = Math.floor(Math.random() * array.length)

    if (randomIndexes.indexOf(randomIndex) === -1) {
      randomIndexes.push(randomIndex)
      objects.push(array[randomIndex])
    }
  }

  return objects
}

type LocationMarkerProps = {
  coordinates: [number, number] | undefined
  setCoordinates: (coordinates: [number, number]) => void
}
const LocationMarker = ({
  coordinates,
  setCoordinates
}: LocationMarkerProps) => {
  const map = useMapEvents({
    click: (e) => {
      setCoordinates([e.latlng.lat, e.latlng.lng])
      map.locate()
    },
    locationfound: (e) => {
      setCoordinates([e.latlng.lat, e.latlng.lng])
      map.flyTo(e.latlng, map.getZoom())
    }
  })

  return coordinates === undefined ? null : (
    <Marker
      icon={L.icon({
        iconUrl: "/pin.svg",
        iconRetinaUrl: "/pin.svg",
        iconSize: [50, 50],
        iconAnchor: [25, 50],
        shadowUrl: "",
        shadowRetinaUrl: ""
      })}
      position={coordinates}
    ></Marker>
  )
}

const MapLeafLet = () => {
  const [coordinates, setCoordinates] = useState<[number, number] | undefined>(
    undefined
  )
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [plants, setPlants] = useState<any>(null)

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    setIsLoading(true)
    e.preventDefault()
    if (!coordinates) return
    const form = new FormData(e.target as HTMLFormElement)
    const formData = Object.fromEntries(form)
    const res = await sendRequest({
      coordinates,
      soil_type: formData["soil-type"] as SoilTypeList
    })

    setIsLoading(false)
    if (!res) return
    console.log(res)
    // router.push({
    //   pathname: "/results",
    //   query: { lat: res.latitude, lng: res.longitude, soil_type: res.soil_type }
    // })
    setPlants(res)
  }
  //w-screen h-screen
  return (
    <>
      <div>
        {plants ? (
          <div className="w-screen flex flex-col gap-14 p-12">
            <div className="flex flex-row gap-1 items-center justify-center">
              <button
                className="buttonlaunch m-auto flex flex-row items-center justify-center gap-1"
                onClick={() => setPlants(null)}
              >
                Nouvelle Recherche
              </button>
              <Link
                className="m-auto flex flex-row items-center justify-center gap-1"
                href="/map"
              >
                <button className="buttonlaunch2 m-auto flex flex-row items-center justify-center gap-1">
                  Nouveau set de Plante{" "}
                  <img src="refresh.svg" style={{ width: 24, height: 24 }} />
                </button>
              </Link>
            </div>

            <div className="flex flex-row justify-center gap-8">
              {randomObjectsFromArray(plants).map((a: any, index: number) => {
                return (
                  <div
                    key={index}
                    className="static w-60 h-96 bg-greenlight rounded-2xl m-auto"
                  >
                    <div className="flex flex-row p-3 text-greentext text-sm justify-between items-center font-semibold">
                      <p>Plante compatible</p>
                      <p>{index + 1}</p>
                    </div>
                    <div className="flex flex-col text-greentext p-3 ">
                      <div className="flex flex-row gap-2 items-center">
                        <Link
                          target={"_blank"}
                          href={`https://en.wikipedia.org/wiki/${a.name}`}
                          className="text-lg font-semibold"
                        >
                          {a.name.replace(/_/g, " ")}
                        </Link>
                        <Link
                          target={"_blank"}
                          href={`https://en.wikipedia.org/wiki/${a.name}`}
                        >
                          <img
                            src="wikipedia.svg"
                            style={{ width: 24, height: 24 }}
                          />
                        </Link>
                      </div>

                      <p className="text-base font-light">{a.familia}</p>
                    </div>
                    <p className="text-greentext text-3xl font-semibold p-3">
                      Plante
                    </p>
                    <div className="absolute">
                      <img
                        src="/monsteracard.png"
                        alt="pot to grow your monstera"
                        className=""
                        style={{ width: 272, height: 232 }}
                      />
                    </div>
                  </div>
                )
              })}

              <Loader />
            </div>
          </div>
        ) : (
          <div className="w-screen h-screen z-0 mt-8 relative">
            <MapContainer
              className="m-auto h-5/6 w-5/6 rounded-2xl zindex"
              center={[48.86, 2.33]}
              zoom={13}
              scrollWheelZoom
              style={{ backgroundColor: "#D9F0E5" }}
            >
              <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                opacity={0.6}
              />

              <LocationMarker {...{ coordinates, setCoordinates }} />
            </MapContainer>

            <form
              className="w-72 rounded-lg shadow-md top-16 left-44 absolute p-4 bgimp flex flex-col gap-4 opacityremover"
              onSubmit={handleSubmit}
            >
              <div className="flex flex-col gap-2 w-60">
                <label
                  className="titlelabel text-md text-gray-500 mb-1"
                  htmlFor="soil-type"
                >
                  Type de sol
                </label>
                <select
                  name="soil-type"
                  className="selectsoil rounded-lg border text-sm border-greeny focus:ring-2 focus:ring-greeny outline-0 inline-0 p-1 pr-2"
                >
                  <option className="hover:bg-greenlight" value="clay">
                    Argile
                  </option>
                  <option value="chalky">Cailloux</option>
                  <option value="sandy">Sableux</option>
                  <option value="silt">Limoneux</option>
                </select>
              </div>
              <div className="flex flex-col justify-start gap-4">
                <div className="flex flex-col justify-start gap">
                  <label
                    className="titlelabel text-md text-gray-500 mb-1"
                    htmlFor="soil-type"
                  >
                    Latitude
                  </label>
                  <span className="text-sm text-gray-500 font-bold">
                    {" "}
                    {coordinates?.[0]}
                  </span>
                </div>
                <div className="flex flex-col justify-start gap">
                  <label
                    className="titlelabel text-md text-gray-500 mb-1"
                    htmlFor="soil-type"
                  >
                    Longitude
                  </label>
                  <span className="text-sm text-gray-500 font-bold">
                    {" "}
                    {coordinates?.[1]}
                  </span>
                </div>
              </div>
              <button className="buttonlaunch m-auto" type="submit">
                <span className="m-auto">
                  {/* {isLoading ? <Loader /> : "Lancer"} */}
                  Lancer
                </span>
              </button>
            </form>
          </div>
        )}
      </div>
    </>
  )
}
export default MapLeafLet
