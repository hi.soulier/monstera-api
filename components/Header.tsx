"use client"
import Link from "next/link"
import Image from "next/image"
import React, { useState } from "react"
import cx from "clsx"

import Monstera_logo from "/public/Monstera_logo.png"

const Header = () => {
  const [isHovering, setIsHovering] = useState(true)
  const [toggleMenu, setToggleMenu] = useState(false)

  return (
    <header className="flex justify-between items-center py-4 px-6 z-[50] relative">
      <div className="flex items-center">
        <Link href="/">
          <Image
            src={Monstera_logo}
            alt="Monstera logo shape M"
            style={{ width: "112px", height: "76px" }}
          />
        </Link>
      </div>
      <div className="flex items-center mr-20">
        <div
          className="cursor-pointer"
          onMouseEnter={() => setIsHovering(false)}
          onMouseLeave={() => setIsHovering(true)}
        >
          <div className="relative">
            <div
              className="space-y-1"
              onClick={() => setToggleMenu(!toggleMenu)}
            >
              <span className="block w-8 h-0.5  bg-gray-600"></span>
              <span
                className={cx(
                  "block w-8 h-0.5 bg-gray-600 transition-transform relative duration-100",
                  isHovering && "-translate-x-1 w-7"
                )}
              ></span>
              <span className="block w-8 h-0.5 bg-gray-600"></span>{" "}
            </div>
          </div>
          {toggleMenu && (
            <div className="absolute z-50 -translate-x-8 mt-2 bg-white w-40">
              <div className="rounded-md shadow-lg">
                <div className="py-1">
                  <Link
                    className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-greenlight focus:outline-none focus:bg-greenlight transition-colors duration-150 ease-in-out"
                    href="/"
                  >
                    Accueil
                  </Link>
                  <Link
                    className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-greenlight focus:outline-none focus:bg-greenlight transition-colors duration-150 ease-in-out"
                    href="/shop"
                  >
                    Shop
                  </Link>

                  <Link
                    className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-greenlight focus:outline-none focus:bg-greenlight transition duration-150 ease-in-out"
                    href="/map"
                  >
                    Map Monstera
                  </Link>
                  <Link
                    href="/api-doc"
                    className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-greenlight focus:outline-none focus:bg-greenlight transition duration-150 ease-in-out"
                  >
                    API Doc
                  </Link>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </header>
  )
}

export default Header
