import React, { useState, useEffect } from "react"

const Loader = () => {
  const [images, setImages] = useState<string[]>([])
  const [currentImage, setCurrentImage] = useState<number>(0)
  const [loading, setLoading] = useState<boolean>(true)

  useEffect(() => {
    setTimeout(() => {
      setLoading(false)
    }, 2600)
  }, [])

  useEffect(() => {
    setImages(["Group-3.png", "Group-2.png", "Group-1.png", "Group.png"])
  }, [])

  useEffect(() => {
    const timer = setTimeout(() => {
      setCurrentImage(currentImage + 1)
    }, 600)
    return () => clearTimeout(timer)
  }, [currentImage])

  return loading ? (
    <div
      className="w-screen h-screen bg-white zindexplus"
      style={{
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)"
      }}
    >
      {images[currentImage] && (
        <img
          className="transition ease-in duration-500"
          style={
            currentImage == 0
              ? {
                  width: 704,
                  height: 333.47,
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: "translate(-50%, -50%)"
                }
              : currentImage == 1
              ? {
                  width: 704,
                  height: 391.7,
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: "translate(-50%, -50%)"
                }
              : currentImage == 2
              ? {
                  width: 704,
                  height: 511.54,
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: "translate(-50%, -50%)"
                }
              : {
                  width: 704,
                  height: 478.72,
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: "translate(-50%, -50%)"
                }
          }
          src={images[currentImage]}
          alt="loader"
        />
      )}
    </div>
  ) : null
}

export default Loader
