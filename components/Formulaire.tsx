import Button from "@/components/Button"
import React from "react"
import Link from "next/link"

const Formulaire = () => {
  const [step, setStep] = React.useState(0)

  const questions = [
    {
      id: 1,
      question: "Possédez-vous une maison ou un appartement  ?"
    },
    {
      id: 2,
      question: "Possédez-vous un jardin ou un espace vert ?"
    },
    {
      id: 3,
      question: "Possédez-vous un bac à jardiner ?"
    },
    {
      id: 4,
      question: "Testez votre sol"
    }
  ]

  const handleSubmit = () => {
    console.log("Valeur de step avant update: " + step)
    setStep(step + 1)
    console.log("Valeur de step apres update: " + step)
  }

  const formStep = () => {
    switch (step) {
      case 0:
        return (
          <>
            <h2 className="project-title">Pour commencer,</h2>
            <div className="formulaire-centered-content">
              {/* STEPS view */}
              <ol className="flex items-center w-full mb-8 sm:mb-[8vh]">
                <li className="flex w-full items-center dark:text-blue-500 after:content-[''] after:w-full after:h-1 after:border-b after:border-[#1e3128] after:border-4 after:inline-block dark:after:border-[#1e3128]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#312e81] rounded-full lg:h-12 lg:w-12 dark:bg-[#312e81] shrink-0">
                    <img
                      src="/home.svg"
                      alt="whats your house type house or appartment"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-[#1e3128] after:border-4 after:inline-block dark:after:border-[#1e3128]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#1e3128] rounded-full lg:h-12 lg:w-12 dark:bg-[#1e3128] shrink-0">
                    <img
                      src="/leaves.svg"
                      alt="do you have a garden"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-100 after:border-4 after:inline-block dark:after:border-gray-700">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#1e3128] rounded-full lg:h-12 lg:w-12 dark:bg-[#1e3128] shrink-0">
                    <img
                      src="/terrarium.svg"
                      alt="do you have a terrarium or garden pot"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex items-center">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#1e3128] rounded-full lg:h-12 lg:w-12 dark:bg-[#1e3128] shrink-0">
                    <img
                      src="/grass.svg"
                      alt="Test your ground type"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
              </ol>
              <p className="formulaire-question">{questions[0].question}</p>
              <div className="flex flex-row justify-around mt-[5vw]">
                <div onClick={handleSubmit}>
                  <Button btn_title="Maison" />
                </div>
                <div onClick={handleSubmit}>
                  <Button btn_title="Appartement" />
                </div>
              </div>
            </div>
          </>
        )
      case 1:
        return (
          <>
            <h2 className="project-title">Pour commencer,</h2>

            <div className="formulaire-centered-content">
              {/* STEPS view */}
              <ol className="flex items-center w-full mb-8 sm:mb-[8vh]">
                <li className="flex w-full items-center dark:text-blue-500 after:content-[''] after:w-full after:h-1 after:border-b after:border-[#312e81] after:border-4 after:inline-block dark:after:border-[#312e81]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#312e81] rounded-full lg:h-12 lg:w-12 dark:bg-[#312e81] shrink-0">
                    <img
                      src="/home.svg"
                      alt="whats your house type house or appartment"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-[#1e3128] after:border-4 after:inline-block dark:after:border-[#1e3128]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#312e81] rounded-full lg:h-12 lg:w-12 dark:bg-[#312e81] shrink-0">
                    <img
                      src="/leaves.svg"
                      alt="do you have a garden"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-100 after:border-4 after:inline-block dark:after:border-gray-700">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#1e3128] rounded-full lg:h-12 lg:w-12 dark:bg-[#1e3128] shrink-0">
                    <img
                      src="/terrarium.svg"
                      alt="do you have a terrarium or garden pot"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex items-center">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#1e3128] rounded-full lg:h-12 lg:w-12 dark:bg-[#1e3128] shrink-0">
                    <img
                      src="/grass.svg"
                      alt="Test your ground type"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
              </ol>
              <p className="formulaire-question">{questions[1].question}</p>
              <div className="flex flex-row justify-around mt-[5vw]">
                <div onClick={handleSubmit}>
                  <Button btn_title="Oui" />
                </div>
                <div onClick={handleSubmit}>
                  <Button btn_title="Non" />
                </div>
              </div>
            </div>
          </>
        )
      case 2:
        return (
          <>
            <h2 className="project-title">Pour commencer,</h2>
            <div className="formulaire-centered-content">
              {/* STEPS view */}
              <ol className="flex items-center w-full mb-8 sm:mb-[8vh]">
                <li className="flex w-full items-center dark:text-blue-500 after:content-[''] after:w-full after:h-1 after:border-b after:border-[#312e81] after:border-4 after:inline-block dark:after:border-[#312e81]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#312e81] rounded-full lg:h-12 lg:w-12 dark:bg-[#312e81] shrink-0">
                    <img
                      src="/home.svg"
                      alt="whats your house type house or appartment"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-[#312e81] after:border-4 after:inline-block dark:after:border-[#312e81]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#312e81] rounded-full lg:h-12 lg:w-12 dark:bg-[#312e81] shrink-0">
                    <img
                      src="/leaves.svg"
                      alt="do you have a garden"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-[#1e3128] after:border-4 after:inline-block dark:after:border-[#1e3128]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#312e81] rounded-full lg:h-12 lg:w-12 dark:bg-[#312e81] shrink-0">
                    <img
                      src="/terrarium.svg"
                      alt="do you have a terrarium or garden pot"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex items-center">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#1e3128] rounded-full lg:h-12 lg:w-12 dark:bg-[#1e3128] shrink-0">
                    <img
                      src="/grass.svg"
                      alt="Test your ground type"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
              </ol>
              <p className="formulaire-question">{questions[2].question}</p>
              <div className="flex flex-row justify-around mt-[5vw]">
                <div onClick={handleSubmit}>
                  <Button btn_title="Oui" />
                </div>
                <div onClick={handleSubmit}>
                  <Button btn_title="Non" />
                </div>
              </div>
            </div>
          </>
        )
      case 3:
        return (
          <>
            <h2 className="project-title">Un dernier effort,</h2>
            <div className="formulaire-centered-content">
              {/* STEPS view */}
              <ol className="flex items-center w-full mb-8 sm:mb-[8vh]">
                <li className="flex w-full items-center dark:text-blue-500 after:content-[''] after:w-full after:h-1 after:border-b after:border-[#312e81] after:border-4 after:inline-block dark:after:border-[#312e81]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#312e81] rounded-full lg:h-12 lg:w-12 dark:bg-[#312e81] shrink-0">
                    <img
                      src="/home.svg"
                      alt="whats your house type house or appartment"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-[#312e81] after:border-4 after:inline-block dark:after:border-[#312e81]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#312e81] rounded-full lg:h-12 lg:w-12 dark:bg-[#312e81] shrink-0">
                    <img
                      src="/leaves.svg"
                      alt="do you have a garden"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-[#312e81] after:border-4 after:inline-block dark:after:border-[#312e81]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#312e81] rounded-full lg:h-12 lg:w-12 dark:bg-[#312e81] shrink-0">
                    <img
                      src="/terrarium.svg"
                      alt="do you have a terrarium or garden pot"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex items-center">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#312e81] rounded-full lg:h-12 lg:w-12 dark:bg-[#312e81] shrink-0">
                    <img
                      src="/grass.svg"
                      alt="Test your ground type"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
              </ol>
              <p className="formulaire-question">{questions[3].question}</p>
              <iframe
                width="100%"
                height="250vh"
                src="https://www.youtube.com/embed/rT8PNkjz638"
                title="YouTube video player"
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                allowFullScreen
              ></iframe>
              <div className="flex flex-row justify-around mt-[5vh]">
                <Link href="/map" className="cursor-pointer">
                  <Button btn_title="Suivant" />
                </Link>
              </div>
            </div>
          </>
        )
      default:
        return (
          <>
            <div className="formulaire-centered-content">
              {/* STEPS view */}
              <ol className="flex items-center w-full mb-8 sm:mb-[10vh]">
                <li className="flex w-full items-center dark:text-blue-500 after:content-[''] after:w-full after:h-1 after:border-b after:border-[#1e3128] after:border-4 after:inline-block dark:after:border-[#1e3128]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#1e3128] rounded-full lg:h-12 lg:w-12 dark:bg-[#1e3128] shrink-0">
                    <img
                      src="/home.svg"
                      alt="whats your house type house or appartment"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-[#1e3128] after:border-4 after:inline-block dark:after:border-[#1e3128]">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#1e3128] rounded-full lg:h-12 lg:w-12 dark:bg-[#1e3128] shrink-0">
                    <img
                      src="/leaves.svg"
                      alt="do you have a garden"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-100 after:border-4 after:inline-block dark:after:border-gray-700">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#1e3128] rounded-full lg:h-12 lg:w-12 dark:bg-[#1e3128] shrink-0">
                    <img
                      src="/terrarium.svg"
                      alt="do you have a terrarium or garden pot"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
                <li className="flex items-center">
                  <div className="flex items-center justify-center w-10 h-10 bg-[#1e3128] rounded-full lg:h-12 lg:w-12 dark:bg-[#1e3128] shrink-0">
                    <img
                      src="/grass.svg"
                      alt="Test your ground type"
                      className="w-5 h-5 lg:w-6 lg:h-6"
                    />
                  </div>
                </li>
              </ol>
              <p className="formulaire-question">{questions[0].question}</p>
              <div className="flex flex-row justify-around mt-[5vw]">
                <Link href="/map" className="cursor-pointer">
                  <Button btn_title="Suivant" />
                </Link>
              </div>
            </div>
          </>
        )
    }
  }

  return <>{formStep()}</>
}

export default Formulaire
