"use client"
import React from "react"

type Props = {
  btn_title: string
}

const Button = (props: Props) => {
  return (
    <div className="btn-wrap">
      <div className="btn-form">
        <label className="btn-title">{props.btn_title}</label>
        <span className="circle"></span>
      </div>
    </div>
  )
}

export default Button
