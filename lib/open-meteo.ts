import { RequestOpenMeteo, ResponseOpenMeteo } from "@/types/api/OpenMeteo"
import axios, { type AxiosResponse } from "axios"

export const formatParamsForOpenMeteoQuery = (
  coordinates: number[],
  endDate = new Date()
) => {
  const startDate = new Date(endDate.getTime())
  startDate.setFullYear(endDate.getFullYear() - 1)

  return {
    latitude: coordinates[0],
    longitude: coordinates[1],
    start_date: startDate.toISOString().split("T")[0],
    end_date: endDate.toISOString().split("T")[0],
    hourly:
      "temperature_2m,precipitation,rain,snowfall,cloudcover,windspeed_10m,soil_temperature_0_to_7cm,soil_moisture_0_to_7cm,direct_radiation",
    timeformat: "unixtime"
  }
}

export const requestOpenMeteo = async (params: RequestOpenMeteo) => {
  try {
    const responseWeatherApi = await axios.get<
      ResponseOpenMeteo,
      AxiosResponse<ResponseOpenMeteo>,
      RequestOpenMeteo
    >("https://archive-api.open-meteo.com/v1/era5", { params })

    return responseWeatherApi
  } catch (error) {
    console.error(error)
    throw error
  }
}
