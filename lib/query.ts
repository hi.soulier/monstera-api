import { queryDatabase } from "@/lib/database"
import { HortipedaTable } from "@/types/database/Hortipeda"
import { FieldPacket } from "mysql2"

export const moistureCloudCoverTemperatureQueries = ({
  moistureValue,
  exposureValue,
  hardinessValue
}: {
  moistureValue: number
  exposureValue: number
  hardinessValue: number
}): Promise<{ rows: HortipedaTable[]; fields: FieldPacket[] }>[] => [
  queryDatabase(
    `SELECT * FROM hortipediadata where moisture like '%${moistureValue}%';`
  ),
  queryDatabase(
    `SELECT * FROM hortipediadata WHERE exposure like '%${exposureValue}%';`
  ),
  queryDatabase(
    `SELECT * FROM hortipediadata WHERE hardiness like '%${hardinessValue}%';`
  )
]

export const queryResultDatabase = async ({
  moistureValue,
  exposureValue,
  hardinessValue
}: {
  moistureValue: number
  exposureValue: number
  hardinessValue: number
}) => {
  return await queryDatabase(
    `SELECT * FROM hortipediadata where moisture like '%${moistureValue}%' AND exposure like '%${exposureValue}%' AND hardiness like '%${hardinessValue}%';`
  )
}
