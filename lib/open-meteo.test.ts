import { formatParamsForOpenMeteoQuery } from "@/lib/open-meteo"

describe("[lib/open-meteo]", () => {
  describe("formatParamsForOpenMeteoQuery", () => {
    it("should return the correct params", () => {
      const coordinates = [52.520008, 13.404954]
      const endDate = new Date("1995-12-17T03:24:00")

      const actualState = formatParamsForOpenMeteoQuery(coordinates, endDate)
      const expectedState = {
        latitude: 52.520008,
        longitude: 13.404954,
        start_date: "1994-12-17",
        end_date: "1995-12-17",
        hourly:
          "temperature_2m,precipitation,rain,snowfall,cloudcover,windspeed_10m,soil_temperature_0_to_7cm,soil_moisture_0_to_7cm,direct_radiation",
        timeformat: "unixtime"
      }

      expect(actualState).toEqual(expectedState)
    })
  })
})
