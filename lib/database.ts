import { HortipedaTable } from "@/types/database/Hortipeda"
import { createPool, FieldPacket } from "mysql2"

export const pool = createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  database: process.env.DB_NAME,
  password: process.env.DB_PASSWORD
})

export const queryDatabase = async (
  query: string,
  values?: any
): Promise<{ rows: HortipedaTable[]; fields: FieldPacket[] }> => {
  try {
    const promisePool = pool.promise()
    promisePool.query(query)
    const [rows, fields] = await promisePool.query<HortipedaTable[]>(
      query,
      values
    )
    return { rows, fields }
  } catch (error) {
    console.error(error)
    throw error
  }
}
