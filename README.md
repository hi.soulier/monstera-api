<div align="center">
<a href="https://aimeos.org/">
    <img src="./public/Monstera_logo.png" alt="Monstera logo" title="Monstera" height="50" />
</a>

  <p align="center">
  Get the right combination of plants for your local environment.    
    <br />
  <a href="https://www.vanaprincipia.fr/"><strong>Vana Principia </strong></a>
    <br />
    <br />    
    <a href="https://github.com/Juba1998">Juba Harfouche</a>
    *
    <a href="https://github.com/AlanDufail">Alan Dufail</a>
    *
    <a href="https://github.com/YanisLQ">Yanis Leclercq</a>
    *
    <a href="https://github.com/hsoulier">Hippolyte Soulier</a>
  </p>
</div>

# Project Monstera

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/hi.soulier/monstera-api?branch=main)
![GitLab](https://img.shields.io/gitlab/license/hi.soulier/monstera-api)
![GitLab stars](https://img.shields.io/gitlab/stars/hi.soulier/monstera-api?logo=Gitlab&logoColor=https%3A%2F%2Fabout.gitlab.com%2Fimages%2Fpress%2Flogo%2Fpng%2Fgitlab-logo-500.png)

![public](/public/Monstera.png)

**[Project Monstera](/) is a POC from 4 french students.**<br/>
The objective is to propose a solution for the vegetation of exteriors and interiors. A certain number of advices and suggestions of plants that can live in the same ecosystem will be proposed to the user.
We using a database of plants scrapped from the [Hortipedia](https://hortipedia.com/) website.

## Table Of Content

- [Installation](#installation)
  - [Deps](#dependencies)
  - [Database setup](#database-setup)
- [License](#license)

<!-- GETTING STARTED -->

## Installation

We use [NextJs](https://nextjs.org) as a framework for our frontend and NextJs also for our API.

- [NextJs](https://nextjs.org) version 13 or later
- [React](https://reactjs.org) version 18 or later
- [NodeJs](https://nodejs.org) version 16 or later

### Dependencies

Install the dependencies with yarn or npm

```bash
yarn install
```

### Database setup

We use a [MySQL](https://www.mysql.com) database for our project.

- [MySQL](https://www.mysql.com) on Cloud (recommended)
- [MySQL](https://www.mysql.com) on your computer with the [SQL dump file](/db_hortipedia.sql)

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License

The MIT License is a permissive free software license originating at the Massachusetts Institute of Technology (MIT) in the late 1980s. As a permissive license, it puts only very limited restriction on reuse and has, therefore, high license compatibility. [More informations](https://en.wikipedia.org/wiki/MIT_License)

License and is available for free.
