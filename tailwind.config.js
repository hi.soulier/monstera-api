/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}"
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ["var(--font-sans)", "sans-serif"]
      },
      colors: {
        greenlight: "#D9F0E5",
        greeny: "#005E35",
        greentext: "#2A7D56"
      }
    }
  },

  plugins: []
}
