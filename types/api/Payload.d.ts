import type { SoilTypeList } from "@/types/Lists"

export type Payload = {
  coordinates: [number, number]
  soil_type: SoilTypeList
}
