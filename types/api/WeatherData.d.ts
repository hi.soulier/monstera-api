import { NextApiRequest, NextApiResponse } from "next"
import { ResponseOpenMeteo } from "./OpenMeteo"

export type WeatherDataRequest = NextApiRequest & {
  body: { coordinates: [number, number] }
}

export type WeatherDataResponse = NextApiResponse<
  ResponseOpenMeteo | { error: string }
>
