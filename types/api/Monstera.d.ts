import { SoilTypeList } from "@/types/Lists"
import { HortipedaTableWithWeight } from "@/utils/sorting"
import { IncomingMessage } from "http"
import { NextApiResponse } from "next"
import { ErrorResponse } from "./Error"

export type MonsteraRequest = IncomingMessage & {
  query: Partial<{ [key: string]: string | string[] }>
  cookies: Partial<{ [key: string]: string }>
  env: Partial<{ [key: string]: string | undefined }>
  preview?: boolean
  previewData?: PreviewData
  body: { coordinates: [number, number]; soil_type: SoilTypeList }
}

export type HortipedaTableWithWeightList = HortipedaTableWithWeight[]

export type MonsteraResponse = NextApiResponse<
  HortipedaTableWithWeightList | ErrorResponse
>
