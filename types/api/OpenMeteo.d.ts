import { SoilTypeList } from "@/types/Lists"

export type ResponseOpenMeteo = {
  latitude: number
  longitude: number
  generationtime_ms: number
  utc_offset_seconds: 0
  timezone: "GMT"
  timezone_abbreviation: "GMT"
  elevation: number
  soil_type: SoilTypeList
  hourly_units: {
    time: "unixtime"
    temperature_2m: "°C"
    precipitation: "mm"
    rain: "mm"
    snowfall: "cm"
    cloudcover: "%"
    windspeed_10m: "km/h"
    soil_temperature_0_to_7cm: "°C"
    soil_moisture_0_to_7cm: "m³/m³"
  }
  hourly: {
    soil_moisture_0_to_7cm: number[]
    time: number[]
    temperature_2m: number[]
    precipitation: number[]
    rain: number[]
    snowfall: number[]
    cloudcover: number[]
    windspeed_10m: number[]
    soil_temperature_0_to_7cm: number[]
  }
}

export type RequestOpenMeteo = {
  latitude: number
  longitude: number
  start_date: string
  end_date: string
  hourly: string
  timeformat: string
}
