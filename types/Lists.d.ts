export type SeasonList = "spring" | "summer" | "fall" | "winter"
export type SoilTypeList = "clay" | "sandy" | "silt" | "loams" | "chalky"
export type FoodTypeList = "vegetable" | "fruit" | "flower"
