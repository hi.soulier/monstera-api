import { RowDataPacket } from "mysql2"

export type HortipedaTable = RowDataPacket & {
  name: string
  author: string
  familia: string
  lifeform: string
  habitus: string
  plantuse: string
  heigh: string
  exposure: string
  moisture: string
  soil: string
  hardiness: string
  leafshape: string
  leafdivision: string
  leafarrangement: string
  leafretention: string
  autumncolou: string
  floweringperiod: string
  flowercolou: string
  flowershape: string
  inflorescence: string
}
